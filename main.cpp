#include <cinttypes>
//#include <cstdio>


#include <iostream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <random>

using namespace std;

void read_from_stdin(vector<unsigned>& weights, vector<unsigned>& values)
{
    cout << "Item weights (space separated) : ";
    for(auto& w : weights)
    {
        cin >> w;
    }

    cout << "Item values (space separated)  : ";
    for(auto& v : values)
    {
        cin >> v;
    }
}

void generate_random(vector<unsigned>& weights, vector<unsigned>& values, unsigned capacity)
{
    random_device rd {};
    default_random_engine re {rd()};
    uniform_int_distribution<unsigned> dist {};

    for(auto& w : weights)
    {
        w = (dist(re) % capacity) + 1;
    }

    cout << "Item values (space separated)  : ";
    for(auto& v : values)
    {
        v = (dist(re) % weights.size()) + 1;
    }
}

int main()
{
    unsigned count {0};
    cout << "Number of items (2..63)        : ";
    cin >> count;
    if ((count < 2) || (count > 63))
    {
        cerr << "Out of range" << endl;
        return 1;
    }

    unsigned capacity {0};
    cout << "Total capacity                 : ";
    cin >> capacity;

    vector<unsigned> weights(count, 9);
    vector<unsigned> values(count, 9);
    //read_from_stdin(weights, values);
    generate_random(weights, values, capacity);

    cout << "Items : ";
    for (unsigned i = 0; i < count; ++i)
    {
        cout << "{" << weights[i] << ", " << values[i] << "} ";
    }
    cout << endl;

    cout << "Start calculation..." << endl;
    unsigned max_value {0};
    unsigned weight_for_max_value {0};
    uint64_t mask_max {0};
    unsigned same_count {0};
    auto start_time{chrono::steady_clock::now()};
    for (uint64_t mask = 0; mask < (1ULL << count); mask++)
    {
        unsigned w{0};
        unsigned v{0};
        for (unsigned i = 0; i < count; i++)
        {
            uint64_t bit {1ull << i};
            if ((mask & bit) == 0ull) {
                continue;
            }
            w += weights[i];
            v += values[i];
        }

        if (w > capacity)
        {
            continue;   // Превысили ёмкость рюкзака.
        }

        if ((v > max_value) || ((v == max_value) && (w > weight_for_max_value)))
        {
            max_value = v;
            weight_for_max_value = w;
            mask_max = mask;
            same_count = 1;
        }
        else if ((v == max_value) && (w == weight_for_max_value))
        {
            ++same_count;
        }
    }

    uint64_t ms = chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - start_time).count();

    cout << "Calculation finished in " << (ms / 1000000) << "."
         << setw(6) << setfill('0') << (ms % 1000000) << " seconds." << endl;

    if (0 == same_count)
    {
        cout << "No solution." << endl;
    }
    else
    {
        cout << "Maximum total value  : " << max_value << endl;
        cout << "Maximum total weight : " << weight_for_max_value << endl;
        cout << "Items                : ";
        for (unsigned i = 0; i < count; i++)
        {
            if ((mask_max & (1ull << i)) == 0ull) {
                continue;
            }
            cout << "{" << weights[i] << ", " << values[i] << "} ";
        }
        cout << endl;

        if (same_count > 1)
        {
            std::cout << "Found " << (same_count - 1) << " sequences with the same values." << endl;
        }
    }


    cout << "Press CTRL+C to close programm." << endl;
    string s;
    cin >> s;

    return 0;
}
